require 'faker'

FactoryGirl.define do
  factory :product do |f|
    f.name { Faker::Commerce.product_name }
    f.price { Faker::Commerce.price }
    f.description { Faker::Lorem.sentence }
  end
end