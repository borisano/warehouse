FactoryGirl.define do
  factory :user do
    email { Faker::Internet.email }
    password 'password'
    password_confirmation 'password'
  end

  factory :admin, parent: :user do
    after(:build) { |user| user.roles = [:admin] }
  end
end
