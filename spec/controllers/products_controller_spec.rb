require 'rails_helper'
require 'support/shared_examples_controller'

RSpec.describe ProductsController, type: :controller do
  describe 'GET #index' do
    def get_index
      get :index
    end

    let(:product) { FactoryGirl.create(:product) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }
      before { get_index }

      it 'assigns products to @products' do
        assigned_products = assigns(:products)

        expect(assigned_products).to include product
      end

      it 'renders correct template' do
        expect(response).to render_template :index
      end
    end

    context 'without authorized user' do
      before { get_index }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #show' do
    def get_show( id=0 )
      get :show, { id: id }
    end

    let(:product) { FactoryGirl.create(:product) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with correct id' do
        before { get_show( product.id ) }

        it 'assigns product to @product' do
          expect(assigns(:product)).to eq product
        end

        it 'renders correct template' do
          expect(response).to render_template :show
        end
      end

      context 'with incorrect id' do
        it 'raises error' do
          expect{
            get_show
          }.to raise_error ActiveRecord::RecordNotFound
        end
      end
    end

    context 'without authorized user' do
      before { get_show( product.id ) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #edit' do
    def get_edit( id=0 )
      get :edit, { id: id }
    end

    let(:product) { FactoryGirl.create(:product) }


    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with correct id' do
        before { get_edit( product.id ) }

        it 'assigns product to @product' do
          expect(assigns(:product)).to eq product
        end

        it 'renders correct template' do
          expect(response).to render_template :edit
        end
      end

      context 'with incorrect id' do
        it 'raises error' do
          expect{
            get_edit
          }.to raise_error ActiveRecord::RecordNotFound
        end
      end
    end

    context 'without authorized user' do
      before { get_edit( product.id ) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #new' do
    def get_new
      get :new
    end

    before { get_new }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }
      before { get_new }

      it 'assings new Product to @product' do
        product = assigns(:product)

        expect(product).to be_a Product
        expect(product).to be_new_record
      end

      it 'renders correct template' do
        expect(response).to render_template :new
      end
    end

    context 'without authorized user' do
      before { get_new }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'POST #create' do
    def post_create(product_info)
      post :create, { product: product_info }
    end

    let(:product_attrs) { FactoryGirl.attributes_for(:product) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with valid data' do
        it 'creates new Product' do
          expect {
            post_create(product_attrs)
          }.to change(Product, :count).by(1)
        end

        describe 'categories association' do
          let!(:category1) { FactoryGirl.create(:category) }
          let!(:category2) { FactoryGirl.create(:category) }

          it 'correctly sets associated categories' do
            post_create product_attrs.merge(category_ids: [category1.to_param])

            expect(Product.last.categories).to include category1
            expect(Product.last.categories).to_not include category2
          end
        end

        it 'redirects to edit' do
          post_create(product_attrs)

          expect(response).to redirect_to Product.last
        end
      end

      context 'with invalid data' do
        let(:wrong_product_attrs) { product_attrs.merge(price: nil) }

        it 'does not create new Product' do
          expect {
            post_create(wrong_product_attrs)
          }.to_not change(Product, :count)
        end

        it 'redirects to new' do
          post_create(wrong_product_attrs)

          expect(response).to render_template :new
        end
      end
    end

    context 'without authorized user' do
      before { post_create(product_attrs) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'PUT #update' do
    def put_update(id = 0, new_attrs = {})
      put :update, { id: id, product: new_attrs }
    end

    let!(:product) { FactoryGirl.create(:product) }
    let(:new_attrs) { FactoryGirl.attributes_for(:product) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with correct product id' do
        context 'with valid data' do
          describe 'product' do
            before { put_update( product, new_attrs ) }

            it 'assigns correct Product' do
              expect(assigns(:product)).to eq product
            end

            it 'updates product attributes' do
              product.reload

              expect(product.price.to_f).to eq new_attrs[:price].to_f
              expect(product.name).to eq new_attrs[:name]
              expect(product.description).to eq new_attrs[:description]
            end

            it 'redirects to product page' do
              expect(response).to redirect_to product
            end
          end


          describe 'categories association' do
            let!(:category1) { FactoryGirl.create(:category) }
            let!(:category2) { FactoryGirl.create(:category) }

            let(:new_attrs_with_category) {
              new_attrs.merge(category_ids: [category1.to_param])
            }

            before { put_update(product, new_attrs_with_category) }

            it 'correctly sets associated categories' do
              product.reload

              expect(product.categories).to include category1
              expect(product.categories).to_not include category2
            end
          end
        end

        context 'with invalid data' do
          let(:wrong_product_attrs) { new_attrs.merge(price: nil) }

          before { put_update( product, wrong_product_attrs ) }

          it 'does not update product attributes' do
            product.reload

            expect(product.price.to_f).to_not eq new_attrs[:price].to_f
            expect(product.name).to_not eq new_attrs[:name]
            expect(product.description).to_not eq new_attrs[:description]
          end

          it 'redirects to edit page' do
            expect(response).to render_template :edit
          end
        end
      end

      context 'with incorrect product id' do
        it 'raises error' do
          expect{
            put_update
          }.to raise_error ActiveRecord::RecordNotFound
        end
      end
    end

    context 'without authorized user' do
      before { put_update( product, new_attrs ) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'DELETE #destroy' do
    def delete_destroy(id=0)
      delete :destroy, {id: id}
    end

    let!(:product) { FactoryGirl.create(:product) }

    context 'with authorized user' do
      context 'with admin user' do
        before { sign_in FactoryGirl.create(:admin) }

        context 'with correct id' do
          it 'deletes product' do
            expect{
              delete_destroy(product.to_param)
            }.to change(Product, :count).by(-1)
          end

          it 'redirects to index page' do
            delete_destroy(product.to_param)

            expect(response).to redirect_to products_url
          end
        end

        context 'with incorrect id' do
          it 'raises error' do
            expect{
              delete_destroy
            }.to raise_error ActiveRecord::RecordNotFound
          end
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create(:user)
          delete_destroy(product.to_param)
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { delete_destroy(product.to_param) }

      it_behaves_like 'unauthorized access'
    end
  end
end
