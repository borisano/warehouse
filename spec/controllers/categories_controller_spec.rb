require 'rails_helper'
require 'support/shared_examples_controller'

RSpec.describe CategoriesController, type: :controller do
  describe 'GET #index' do
    def get_index
      get :index
    end
    let!(:category) { FactoryGirl.create(:category) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      before { get_index }

      it 'assigns categories to @categories' do
        expect(assigns(:categories)).to include category
      end

      it 'renders correct template' do
        expect(response).to render_template :index
      end
    end

    context 'without authorized user' do
      before { get_index }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #show' do
    def get_show( id=0 )
      get :show, {id: id}
    end

    let(:category) { FactoryGirl.create(:category) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with correct id' do
        before { get_show( category.id ) }

        it 'assigns category to @category' do
          expect(assigns(:category)).to eq category
        end

        it 'renders correct template' do
          expect(response).to render_template :show
        end
      end

      context 'with incorrect id' do
        it 'raises error' do
          expect{
            get_show
          }.to raise_error ActiveRecord::RecordNotFound
        end
      end
    end

    context 'without authorized user' do
      before { get_show(category.to_param) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #edit' do
    def get_edit( id=0 )
      get :edit, { id: id }
    end

    let(:category) { FactoryGirl.create(:category) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with correct id' do
        before { get_edit( category.id ) }

        it 'assigns category to @category' do
          expect(assigns(:category)).to eq category
        end

        it 'renders correct template' do
          expect(response).to render_template :edit
        end
      end

      context 'with incorrect id' do
        it 'raises error' do
          expect{
            get_edit
          }.to raise_error ActiveRecord::RecordNotFound
        end
      end
    end

    context 'without authorized user' do
      before { get_edit(category.to_param) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #new' do
    def get_new
      get :new
    end

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }
      before { get_new }

      it 'assings new Category to @category' do
        category = assigns(:category)

        expect(category).to be_a Category
        expect(category).to be_new_record
      end

      it 'renders correct template' do
        expect(response).to render_template :new
      end
    end

    context 'without authorized user' do
      before { get_new }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'POST #create' do
    def post_create(category_info = {})
      post :create, { category: category_info }
    end

    let(:category_attrs) { FactoryGirl.attributes_for(:category) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with valid data' do
        it 'creates new Category' do
          expect {
            post_create(category_attrs)
          }.to change(Category, :count).by(1)
        end

        it 'redirects to edit' do
          post_create(category_attrs)

          expect(response).to redirect_to Category.last
        end
      end

      context 'with invalid data' do
        let(:wrong_category_attrs) { category_attrs.merge(name: nil) }

        it 'does not create new Category' do
          expect {
            post_create(wrong_category_attrs)
          }.to_not change(Category, :count)
        end

        it 'redirects to new' do
          post_create(wrong_category_attrs)

          expect(response).to render_template :new
        end
      end
    end

    context 'without authorized user' do
      before { post_create(category_attrs) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'PUT #update' do
    def put_update(id = 0, new_attrs = {})
      put :update, { id: id, category: new_attrs }
    end

    let!(:category) { FactoryGirl.create(:category) }
    let(:new_attrs) { FactoryGirl.attributes_for(:category) }

    context 'with authorized user' do
      before { sign_in FactoryGirl.create(:user) }

      context 'with correct category id' do
        context 'with valid data' do
          before { put_update( category, new_attrs ) }

          it 'assigns correct Product' do
            expect(assigns(:category)).to eq category
          end

          it 'updates category attributes' do
            category.reload

            expect(category.name).to eq new_attrs[:name]
          end

          it 'redirects to category page' do
            expect(response).to redirect_to category
          end
        end

        context 'with invalid data' do
          let(:wrong_category_attrs) { new_attrs.merge(name: nil) }

          before { put_update( category, wrong_category_attrs ) }

          it 'does not update category attributes' do
            category.reload

            expect(category.name).to_not eq new_attrs[:name]
          end

          it 'redirects to edit page' do
            expect(response).to render_template :edit
          end
        end
      end

      context 'with incorrect category id' do
        it 'raises error' do
          expect{
            put_update
          }.to raise_error ActiveRecord::RecordNotFound
        end
      end
    end

    context 'without authorized user' do
      before { put_update( category, new_attrs ) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'DELETE #destroy' do
    def delete_destroy(id=0)
      delete :destroy, {id: id}
    end

    let!(:category) { FactoryGirl.create(:category) }

    context 'with authorized user' do
      context 'with admin user' do
        before { sign_in FactoryGirl.create(:admin) }

        context 'with correct id' do
          it 'deletes category' do
            expect{
              delete_destroy(category.to_param)
            }.to change(Category, :count).by(-1)
          end

          it 'redirects to index page' do
            delete_destroy(category.to_param)

            expect(response).to redirect_to categories_url
          end
        end

        context 'with incorrect id' do
          it 'raises error' do
            expect{
              delete_destroy
            }.to raise_error ActiveRecord::RecordNotFound
          end
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create :user
          delete_destroy(category.to_param)
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { delete_destroy(category.to_param) }

      it_behaves_like 'unauthorized access'
    end
  end
end
