require 'rails_helper'
require 'support/shared_examples_controller'

RSpec.describe UsersController, type: :controller do
  describe 'GET #index' do
    def get_index
      get :index
    end

    context 'with authorized user' do
      let!(:user) { FactoryGirl.create(:user) }
      context 'with admin user' do
        before do
          sign_in FactoryGirl.create(:admin)
          get_index
        end

        it 'is successful' do
          expect(response).to be_success
        end

        it 'assigns users to @users' do
          expect(assigns(:users)).to include user
        end

        it 'renders correct template' do
          expect(response).to render_template :index
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create(:user)
          get_index
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { get_index }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #show' do
    def get_show( id=0 )
      get :show, {id: id}
    end

    let!(:user) {FactoryGirl.create :user }

    context 'with authorized user' do
      context 'with admin user' do
        before do
          sign_in FactoryGirl.create :admin
          get_show(user.to_param)
        end

        context 'with correct id' do
          it 'assigns user to @user' do
            expect(assigns(:user)).to eq user
          end

          it 'renders correct template' do
            expect(response).to render_template :show
          end
        end

        context 'with incorrect id' do
          it 'raises error' do
            expect{
              get_show
            }.to raise_error ActiveRecord::RecordNotFound
          end
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create(:user)
          get_show
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { get_show(user.to_param) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #edit' do
    def get_edit(id=0)
      get :edit, {id: id}
    end

    let!(:user) { FactoryGirl.create :user }

    context 'with authorized user' do
      context 'with admin user' do
        before do
          sign_in FactoryGirl.create :admin
          get_edit(user.to_param)
        end

        it 'addigns user to @user' do
          expect(assigns(:user)).to eq user
        end

        it 'renders correct template' do
          expect(response).to render_template :edit
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create(:user)
          get_edit(user.to_param)
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { get_edit(user.to_param) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'GET #new' do
    def get_new
      get :new
    end

    context 'with authorized user' do
      context 'with admin user' do
        before do
          sign_in FactoryGirl.create :admin
          get_new
        end

        it 'assigns new User to @user' do
          user = assigns(:user)

          expect(user).to be_an User
          expect(user).to be_new_record
        end

        it 'renders correct template' do
          expect(response).to render_template :new
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create(:user)
          get_new
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { get_new }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'POST #create' do
    def post_create(user_info = {})
      post :create, {user: user_info}
    end

    let(:user_attrs) { FactoryGirl.attributes_for(:user) }

    context 'with authorized user' do
      context 'with admin user' do
        before { sign_in FactoryGirl.create :admin}

        context 'with valid data' do
          it 'creates new User' do
            expect {
              post_create(user_attrs)
            }.to change(User, :count).by(1)
          end

          it 'correctly redirects' do
            post_create(user_attrs)

            expect(response).to redirect_to User.last
          end
        end

        context 'with invalid data' do
          let(:wrong_user_attrs) { user_attrs.merge(email: nil) }

          it 'does not create new User' do
            expect {
              post_create(wrong_user_attrs)
            }.to_not change(User, :count)
          end

          it 'redirects to new' do
            post_create(wrong_user_attrs)

            expect(response).to render_template :new
          end
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create(:user)
          post_create(user_attrs)
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { post_create(user_attrs) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'PUT #update' do
    def put_update(id=0, new_attrs={})
      put :update, {id: id, user: new_attrs }
    end

    let!(:user) { FactoryGirl.create :user }
    let(:new_attrs) { FactoryGirl.attributes_for :user }

    context 'with authorized user' do
      context 'with admin user' do
        before { sign_in FactoryGirl.create :admin }

        context 'with valid data' do
          before { put_update(user.to_param, new_attrs) }

          it 'assings correct User' do
            expect(assigns(:user)).to eq user
          end

          it 'updates user info' do
            user.reload

            expect(user.email).to eq new_attrs[:email]
          end

          it 'redirects to user page' do
            expect(response).to redirect_to user
          end
        end

        context 'with invalid data' do
          let(:wrong_user_attrs) { new_attrs.merge(email: nil) }

          before { put_update(user.to_param, wrong_user_attrs) }

          it 'does not update user attributes' do
            user.reload

            expect(user.email).to_not be_nil
          end

          it 'redirects to edit page' do
            expect(response).to render_template :edit
          end
        end
      end

      context 'with regular user' do
        before do
          sign_in FactoryGirl.create(:user)
          put_update(user.to_param, new_attrs)
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { put_update( user.to_param, put_update(user.to_param, new_attrs) ) }

      it_behaves_like 'unauthorized access'
    end
  end

  describe 'DELETE #destroy' do
    def delete_destroy(id=0)
      delete :destroy, {id: id}
    end

    let!(:user) { FactoryGirl.create(:user) }

    context 'with authorized user' do
      context 'with admin user' do
        before { sign_in FactoryGirl.create :admin }

        context 'with correct id' do
          it 'deletes user' do
            expect{
              delete_destroy(user.to_param)
            }.to change(User, :count).by(-1)
          end

          it 'redirects to index page' do
            delete_destroy(user.to_param)

            expect(response).to redirect_to users_url
          end
        end

        context 'with incorrect id' do
          it 'raises error' do
            expect{
              delete_destroy
            }.to raise_error ActiveRecord::RecordNotFound
          end
        end
      end

      context 'with regular user' do
        before do
         sign_in FactoryGirl.create :user
         delete_destroy(user.to_param)
        end

        it_behaves_like 'not allowed to do action'
      end
    end

    context 'without authorized user' do
      before { delete_destroy(user.to_param) }

      it_behaves_like 'unauthorized access'
    end
  end
end
