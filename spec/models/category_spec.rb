require 'rails_helper'

RSpec.describe Category, type: :model do
  describe 'factory' do
    let(:category) { FactoryGirl.build :category }

    it 'is valid' do
      expect(category).to be_valid
    end
  end

  describe 'validators' do
    let(:category) { FactoryGirl.build :category }

    describe 'name' do
      it 'is required' do
        category.name = nil
        expect(category).to_not be_valid
      end
    end
  end
end
