require 'rails_helper'

RSpec.describe Product, type: :model do

  describe 'factory' do
    let(:product) { FactoryGirl.build :product }

    it 'is valid' do
      expect(product).to be_valid
    end
  end

  describe 'validators' do
    let(:product) { FactoryGirl.build :product }

    describe 'price' do
      it 'is required' do
        product.price = nil
        expect(product).to_not be_valid
      end

      it 'only allows numbers' do
        product.price = 'blah'
        expect(product).to_not be_valid
      end
    end

    describe 'name' do
      it 'is required' do
        product.name = nil
        expect(product).to_not be_valid
      end
    end
  end
end
