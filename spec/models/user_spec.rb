require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'factory' do
    context 'user\s factory' do
      let(:user) { FactoryGirl.build :user }

      it 'is valid' do
        expect(user).to be_valid
      end

      it 'has :user role' do
        expect(user.roles).to include :user
      end

      it 'does not have admin role' do
        expect(user.roles).to_not include :admin
      end
    end

    context 'admin\s factory' do
      let(:admin) { FactoryGirl.build :admin }

      it 'is valid' do
        expect(admin).to be_valid
      end

      it 'has user role' do
        expect(admin.roles).to include :user
      end

      it 'has :admin role' do
        expect(admin.roles).to include :admin
      end
    end

  end

  describe 'validators' do
    let(:user) { FactoryGirl.build :user }

    describe 'email' do
      it 'is required' do
        user.email = nil
        expect(user).to_not be_valid
      end

      it 'validates email' do
        user.email = 'ivalid_email'
        expect(user).to_not be_valid
      end
    end
  end
end
