RSpec.shared_examples 'unauthorized access' do
  it 'redirects to login' do
    expect(response).to redirect_to new_user_session_path
  end
end

RSpec.shared_examples 'not allowed to do action' do
  it 'redirects to root' do
    expect(response).to redirect_to root_path
  end
end