module ControllerMacros
  def login_user(user = nil)
    user ||= FactoryGirl.create(:user)

    @request.env["devise.mapping"] = Devise.mappings[:user]

    sign_in user
  end
end