RSpec.configure do |config|
  # Add devise helpers for controller tests
  config.include Devise::TestHelpers, :type => :controller
  config.extend ControllerMacros, :type => :controller
end