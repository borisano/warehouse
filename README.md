##Task 2

Write from scratch an application in Ruby on Rails called “Warehouse”.


###Description
There is warehouse with many employees who can add/edit/delete products to stock. Also there are
many categories of products. Such case is possible that one products belongs to many categories.
There should be possibility to add and remove new employees to the system. Product should
contain picture, description, price and whatever.

###Summary
We'd like to see from your work:

  * Ruby on Rails 4.x
  * SQL-based database SQLite, Postgres, MySQL.
  * AJAX
  * Clean HTML and CSS.
  * Publish on GitHub
