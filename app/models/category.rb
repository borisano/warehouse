class Category < ActiveRecord::Base
  self.per_page = 5

  validates :name, presence: true

  has_and_belongs_to_many :products
end
