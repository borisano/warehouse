require 'carrierwave/orm/activerecord'

class Product < ActiveRecord::Base
  self.per_page = 5

  validates :name, presence: true
  validates :price, presence: true, numericality: true

  mount_uploader :image, ImageUploader

  has_and_belongs_to_many :categories
end
