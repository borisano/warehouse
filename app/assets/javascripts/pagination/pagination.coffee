$(document).ready ->
  $(document.body).on 'click', 'ul.pagination a', (e) ->
    e.preventDefault()
    $("ul.pagination").html("Page is loading...")
    href = @href
    $.ajax
      url: href
      dataType: 'script'
      success: ->
        history.pushState(null, null, href)