module ApplicationHelper
  def bootstrap_pagination_for(collection)
    will_paginate collection, renderer: BootstrapPagination::Rails
  end
end
