class ProductsController < ApplicationController
  before_action :authenticate_user!
  access user: {except: [:destroy]}, admin: :all

  before_action :set_product, only: [:show, :edit, :update, :destroy]

  def index
    @products = Product.includes(:categories)
                        .paginate(:page => params[:page])
  end

  def show
  end

  def edit
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)

    if @product.save
      redirect_to @product, notice: 'Product was successfully added'
    else
      render :new
    end
  end

  def update
    if @product.update(product_params)
      redirect_to @product, notice: 'Product was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @product.destroy
    redirect_to products_path, notice: 'Product was successfully destroyed.'
  end

  protected
  def set_product
    @product = Product.find(params[:id])
  end

  def product_params
    params.require(:product).permit(
      :name,
      :price,
      :description,

      :image,
      :image_cache,

      category_ids: []
    )
  end
end
